import $ from 'jquery';
import jssha from 'jssha';

export default ()=>{
  function log(e){
    let l = $('#user').val();
    let p = $('#pass-user').val();
    let ss = new jssha('SHA-512',"TEXT");
    ss.update(p);
    let has = ss.getHash("HEX");
    $.ajax('/session-ini',{
      type: 'POST',
      dataType: 'json',
      data: { l: l, has: has },
      success: (data)=>{
        if(data == 'u'){
          $('#dato').html('<span class="mensaje-error">El usuario es incorrecto</span>');
        }
        if(data == 'c'){
          $('#dato').html('<span class="mensaje-error">La contraseña es incorrecta</span>');
        }
        if(data == '=>'){
          document.location = '/administracion-servicio';
        }
      }
    });
    e.preventDefault();
  }
  if(localStorage['lugar'] == 'Login'){
    document.getElementById('in-session').addEventListener('submit',log);
  }
}
