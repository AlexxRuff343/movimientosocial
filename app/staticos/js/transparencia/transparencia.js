import $ from 'jquery';
import moment from 'moment';

export default ()=>{
  if(localStorage['lugar'] == 'transparencia'){
    function incisos(e){
      $.get('/' + e.target.id,(data)=>{
        $('#contenedor-lista-articulo').html(data);
        localStorage['contenedor'] = e.target.id;
      });
      e.preventDefault();
    }

    $(".item-articlulos-at").click(incisos);

    $(".year-informacion").html(moment().year());
    $("#year-reserva").val(moment().year());
    $("#year-reserva-no").val(moment().year());
    $("#year-b").val(moment().year());
    $("#year-b2").val(moment().year());
    $("#year-b3").val(moment().year());
    $("#year-b4").val(moment().year());
    $("#year-b5").val(moment().year());
    $("#year-b6").val(moment().year());
    $("#actualiza-year-reserva").val(moment().year());
  }
}
