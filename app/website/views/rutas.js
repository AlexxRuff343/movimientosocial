var log   = require('../controllers/login'),
    den   = require('../controllers/activa'),
    rutas = require('../controllers/rutasAdmin'),
    transparenciaR = require('../controllers/transparencia'),
    moment=require('moment');

function Rutas(config){
  config = config || {};

  //Gets
  config.app.get('/',(sol,res,next) => {
    res.render('entrada');
  });

  config.app.get('/historia',(sol,res,next)=>{
    res.render('historia');
  });

  config.app.get('/comision-operativa-nacional',(sol,res,next)=>{
    res.render('comision-operativa-nacional');
  });

  config.app.get('/comision-operativa-estatal',(sol,res,next)=>{
    res.render('comision-operativa-estatal');
  });

  config.app.get('/consejo-ciudadano-estatal',(sol,res,next)=>{
    res.render('consejo-ciudadano-estatal');
  });
  
  config.app.get('/coordinadora-ciudadana-estatal',(sol,res,next)=>{
    res.render('coordinadora-ciudadana-estatal');
  });

  config.app.get('/reglamentos',(sol,res,next)=>{
    res.render('reglamentos');
  });

  config.app.get('/plataforma-electoral',(sol,res,next)=>{
    res.render('plataforma-electoral');
  });

  config.app.get('/documentos-basicos',(sol,res,next)=>{
    res.render('documentos-basicos');
  });

  config.app.get('/organigrama',(sol,res,next)=>{
    res.render('organigrama');
  });

  config.app.get('/directorio-estatal',(sol,res,next)=>{
    res.render('directorio-estatal');
  });
  config.app.get('/session-admin',den.segmento,(sol,res,next)=>{
    res.render('login');
  });

  config.app.get('/administracion-servicio',den.iniciado,(sol,res,next)=>{
    res.render('manejo');
  });

  config.app.get('/transparencia',(sol,res,next)=>{
    res.render('transparencia');
  });

  config.app.get('/transparencia-opciones',(sol,res,next)=>{
    res.render('transparencia/transparencia');
  });

  config.app.get('/google996b0ceb9d53e32f.html',(sol,res,next)=>{
    res.render('google996b0ceb9d53e32f');
  });

  var transparenciasection = new transparenciaR(config);

  var ruteo = new rutas(config);

  //Posts
  config.app.post('/session-ini',(sol,res,next)=>{
    var datos = {
      sol: sol,
      res: res
    }
    var logeo = new log(datos);
  });


}

module.exports = Rutas;
