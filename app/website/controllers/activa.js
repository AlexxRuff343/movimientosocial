
var datos = {
  iniciado: (sol,res,next)=>{
    if(sol.session.name){
      next();
    }else{
      res.redirect('/');
    }//Este es para que el usuario si inicio sesion pueda hacer uso de las paginas restringidas para las sesiones
  },
  denegade: (sol,res,next)=>{
    if(!sol.session.name){
      next();
    }else{
      res.redirect('/administracion-servicio');
    }
  },
  segmento: (sol,res,next)=>{
    if(sol.session.name){
      res.redirect('/administracion-servicio');
    }else{
      next();
    }
  }//Este es en caso de que la sesion del usuario este iniciada el usuario no pueda acceder a la pagina para iniciar de sesion de nuevo
}

module.exports = datos;
