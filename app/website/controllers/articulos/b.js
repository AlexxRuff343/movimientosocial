var mongoose = require('mongoose'),
    schema   = require('../../models/db/schema');

var b = function(config){
  config = config || {};
  if(config.sol.body.valores == '1'){
    if(config.sol.body.actun == 'Actualizacion'){

      
      schema.b1.findOne({year:config.sol.body.yearb,trimestre:config.sol.body.trimestreb},(err,data)=>{
        if(err){
          throw err;
        }
        var dato = {
          year: config.sol.body.yearb,
          trimestre: config.sol.body.trimestreb,
          lugar: config.sol.files[0].destination,
          nombre: config.sol.files[0].filename,
          nombrer: config.sol.files[0].originalname
        }
        schema.b1.update({year:dato.year,trimestre:dato.trimestre},dato,(err)=>{
          if(err){
            throw err;
          }
          config.res.json('=>');
        });
      });
    }
    if(config.sol.body.actun == 'Actual'){
      
      if(config.sol.files[0] != undefined){
        var dato = new schema.b1({
          year: config.sol.body.yearb,
          trimestre: config.sol.body.trimestreb,
          lugar: config.sol.files[0].destination,
          nombre: config.sol.files[0].filename,
          nombrer: config.sol.files[0].originalname
        });        
      }else{
        var dato = new schema.b1({
          year: config.sol.body.yearb,
          trimestre: config.sol.body.trimestreb,
          lugar: '',
          nombre: 'En este trimestre no se ha generado información al respecto',
          nombrer: ''
        });
      }    

      
      schema.b1.findOne({year:config.sol.body.yearb,trimestre:config.sol.body.trimestreb},(err,data)=>{
        if(err){
          throw err;
        }

        if(data == null){
          dato.save((err)=>{
            if(err){
              throw err;
            }
            config.res.json('=>');
          });
        }else{
          config.res.json('<=');
        }
      });
    }

    if(config.sol.body.actun == 'noinfo'){
      var dato = {
        year: config.sol.body.yearb,
        trimestre: config.sol.body.trimestreb,
        lugar: '',
        nombre: 'En este trimestre no se ha generado información al respecto',
        nombrer: ''
      }
      schema.b1.update({year:dato.year,trimestre:dato.trimestre},dato,(err)=>{
        if(err){
          throw err;
        }
        config.res.json('=>');
      });
    }
  }

  if(config.sol.body.valores == '2'){
    if(config.sol.body.actun == 'Actualizacion'){
      var dato = {
        year: config.sol.body.yearb,
        trimestre: config.sol.body.trimestreb,
        lugar: config.sol.files[0].destination,
        nombre: config.sol.files[0].filename,
        nombrer: config.sol.files[0].originalname
      }
      schema.b2.update({year:dato.year,trimestre:dato.trimestre},dato,(err)=>{
        if(err){
          throw err;
        }
        config.res.json('=>');
      });
    }
    if(config.sol.body.actun == 'Actual'){
      if(config.sol.files[0] != undefined){      
        var dato = new schema.b2({
          year: config.sol.body.yearb,
          trimestre: config.sol.body.trimestreb,
          lugar: config.sol.files[0].destination,
          nombre: config.sol.files[0].filename,
          nombrer: config.sol.files[0].originalname
        });        
      }else{
        var dato = new schema.b2({
          year: config.sol.body.yearb,
          trimestre: config.sol.body.trimestreb,
          lugar: '',
          nombre: 'En este trimestre no se ha generado información al respecto',
          nombrer: ''
        });
      }

      
      schema.b2.findOne({year:config.sol.body.yearb,trimestre:config.sol.body.trimestreb},(err,data)=>{
        if(err){
          throw err;
        }

        if(data == null){
          dato.save((err)=>{
            if(err){
              throw err;
            }
            config.res.json('=>');
          });
        }else{
          config.res.json('<=');
        }
      });
    }

    if(config.sol.body.actun == 'noinfo'){
      var dato = {
        year: config.sol.body.yearb,
        trimestre: config.sol.body.trimestreb,
        lugar: '',
        nombre: 'En este trimestre no se ha generado información al respecto',
        nombrer: ''
      }
      schema.b2.update({year:dato.year,trimestre:dato.trimestre},dato,(err)=>{
        if(err){
          throw err;
        }
        config.res.json('=>');
      });
    }

  }

  if(config.sol.body.valores == '3'){
    if(config.sol.body.actun == 'Actualizacion'){
      var dato = {
        year: config.sol.body.yearb,
        trimestre: config.sol.body.trimestreb,
        lugar: config.sol.files[0] == undefined ? '' : config.sol.files[0].destination,
        nombrej1: config.sol.files[0] == undefined ? '' : config.sol.files[0].filename,
        nombrejr1: config.sol.files[0] == undefined ? '' : config.sol.files[0].originalname,
        nombrej2: config.sol.files[1] == undefined ? '' : config.sol.files[1].filename,
        nombrej2r: config.sol.files[1] == undefined ? '' : config.sol.files[1].originalname,
        nombrej3: config.sol.files[2] == undefined ? '' : config.sol.files[2].filename,
        nombrej3r: config.sol.files[2] == undefined ? '' : config.sol.files[2].originalname,
        nombrej4: config.sol.files[3] == undefined ? '' : config.sol.files[3].filename,
        nombrej4r: config.sol.files[3] == undefined ? '' : config.sol.files[3].originalname
      }

      schema.b3.update({year:dato.year,trimestre:dato.trimestre},dato,(err)=>{
        if(err){
          throw err;
        }

        config.res.json('=>');
      });
    }

    if(config.sol.body.actun == 'Actual'){
      
      if(config.sol.files != undefined){
        var dato = new schema.b3({
          year: config.sol.body.yearb,
          trimestre: config.sol.body.trimestreb,
          lugar: config.sol.files[0] == undefined ? '' : config.sol.files[0].destination,
          nombrej1: config.sol.files[0] == undefined ? '' : config.sol.files[0].filename,
          nombrejr1: config.sol.files[0] == undefined ? '' : config.sol.files[0].originalname,
          nombrej2: config.sol.files[1] == undefined ? '' : config.sol.files[1].filename,
          nombrej2r: config.sol.files[1] == undefined ? '' : config.sol.files[1].originalname,
          nombrej3: config.sol.files[2] == undefined ? '' : config.sol.files[2].filename,
          nombrej3r: config.sol.files[2] == undefined ? '' : config.sol.files[2].originalname,
          nombrej4: config.sol.files[3] == undefined ? '' : config.sol.files[3].filename,
          nombrej4r: config.sol.files[3] == undefined ? '' : config.sol.files[3].originalname
        });
      }else{
        var dato = new schema.b3({
          year: config.sol.body.yearb,
          trimestre: config.sol.body.trimestreb,
          lugar: '',
          nombrej1: "En este trimestre no se ha generado información al respecto",
          nombrejr1: '',
          nombrej2: '',
          nombrej2r: '',
          nombrej3: '',
          nombrej3r: '',
          nombrej4: '',
          nombrej4r: ''
        });
      }

      
      schema.b3.findOne({year:config.sol.body.yearb,trimestre:config.sol.body.trimestreb},(err,data)=>{
        if(err){
          throw err;
        }

        if(data == null){
          dato.save((err)=>{
            if(err){
              throw err;
            }
            config.res.json('=>');
          });
        }else{
          config.res.json('<=');
        }
      });
    }

    if(config.sol.body.actun == 'noinfo'){
      var dato = {
        year: config.sol.body.yearb,
        trimestre: config.sol.body.trimestreb,
        lugar: '',
        nombrej1: "En este trimestre no se ha generado información al respecto",
        nombrejr1: '',
        nombrej2: '',
        nombrej2r: '',
        nombrej3: '',
        nombrej3r: '',
        nombrej4: '',
        nombrej4r: ''
      }

      schema.b3.update({year:dato.year,trimestre:dato.trimestre},dato,(err)=>{
        if(err){
          throw err;
        }
        config.res.json('=>');
      });
    }

  }

  if(config.sol.body.valores == '4'){
    if(config.sol.body.actun == 'Actualizacion'){
      var dato = {
        year: config.sol.body.yearb,
        trimestre: config.sol.body.trimestreb,
        lugar: config.sol.files[0] == undefined ? '' : config.sol.files[0].destination ,
        nombrej1: config.sol.files[0] == undefined ? '' : config.sol.files[0].filename,
        nombrejr1: config.sol.files[0] == undefined ? '' : config.sol.files[0].originalname,
        nombre: config.sol.files[0] == undefined ? '': config.sol.files[0].filename,
        nombrer: config.sol.files[0] == undefined ? '' : config.sol.files[0].originalname,
        nombre2: config.sol.files[1] == undefined ? '' : config.sol.files[1].filename,
        nombre2r: config.sol.files[1] == undefined ? '' : config.sol.files[1].originalname
      }

      schema.b4.update({year:dato.year,trimestre:dato.trimestre},dato,(err)=>{
        if(err){
          throw err;
        }
        config.res.json('=>');
      });
    }
    if(config.sol.body.actun == 'Actual'){
      if(config.sol.files != undefined){
        var dato = new schema.b4({
          year: config.sol.body.yearb,
          trimestre: config.sol.body.trimestreb,
          lugar: config.sol.files[0] == undefined ? '' : config.sol.files[0].destination ,
          nombrej1: config.sol.files[0] == undefined ? '' : config.sol.files[0].filename,
          nombrejr1: config.sol.files[0] == undefined ? '' : config.sol.files[0].originalname,
          nombre: config.sol.files[0] == undefined ? '': config.sol.files[0].filename,
          nombrer: config.sol.files[0] == undefined ? '' : config.sol.files[0].originalname,
          nombre2: config.sol.files[1] == undefined ? '' : config.sol.files[1].filename,
          nombre2r: config.sol.files[1] == undefined ? '' : config.sol.files[1].originalname
        });
      }else{
        var dato = new schema.b4({
          year: config.sol.body.yearb,
          trimestre: config.sol.body.trimestreb,
          lugar: '',
          nombre: 'En este trimestre no se ha generado información al respecto',
          nombrer: '',
          nombre2: '',
          nombre2r: ''
        });
      }
      
      schema.b4.findOne({year:config.sol.body.yearb,trimestre:config.sol.body.trimestreb},(err,data)=>{
        if(err){
          throw err;
        }

        if(data == null){
          dato.save((err)=>{
            if(err){
              throw err;
            }
            config.res.json('=>');
          });
        }else{
          config.res.json('<=');
        }
      });
    }
    if(config.sol.body.actun == 'noinfo'){
      var dato = {
        year: config.sol.body.yearb,
        trimestre: config.sol.body.trimestreb,
        lugar: '',
        nombre: 'En este trimestre no se ha generado información al respecto',
        nombrer: '',
        nombre2: '',
        nombre2r: ''
      }

      schema.b4.update({year:dato.year,trimestre:dato.trimestre},dato,(err)=>{
        if(err){
          throw err;
        }
        config.res.json('=>');
      });
    }
  }

  if(config.sol.body.valores == '5'){

    if(config.sol.body.actun == 'Actualizacion'){
      var dato = {
        year: config.sol.body.yearb,
        trimestre: config.sol.body.trimestreb,
        lugar: config.sol.files[0].destination,
        nombre: config.sol.files[0].filename,
        nombrer: config.sol.files[0].originalname
      }

      schema.b5.update({year:dato.year,trimestre:dato.trimestre},dato,(err)=>{
        if(err){
          throw err;
        }

        config.res.json('=>');
      });
    }
    if(config.sol.body.actun == 'Actual'){
      if(config.sol.files[0] != undefined){
        var dato = new schema.b5({
          year: config.sol.body.yearb,
          trimestre: config.sol.body.trimestreb,
          lugar: config.sol.files[0].destination,
          nombre: config.sol.files[0].filename,
          nombrer: config.sol.files[0].originalname
        });
      }else{
        var dato = new schema.b5({
          year: config.sol.body.yearb,
          trimestre: config.sol.body.trimestreb,
          lugar: '',
          nombre: 'En este trimestre no se ha generado información al respecto',
          nombrer: ''
        });
      }

      
      schema.b5.findOne({year:config.sol.body.yearb,trimestre:config.sol.body.trimestreb},(err,data)=>{
        if(err){
          throw err;
        }

        if(data == null){
          dato.save((err)=>{
            if(err){
              throw err;
            }
            config.res.json('=>');
          });
        }else{
          config.res.json('<=');
        }
      });
    }
    if(config.sol.body.actun == 'noinfo'){
      var dato = {
        year: config.sol.body.yearb,
        trimestre: config.sol.body.trimestreb,
        lugar: '',
        nombre: 'En este trimestre no se ha generado información al respecto',
        nombrer: ''
      }

      schema.b5.update({year:dato.year,trimestre:dato.trimestre},dato,(err)=>{
        if(err){
          throw err;
        }
        config.res.json('=>');
      });
    }
  }

  if(config.sol.body.valores == '6'){

    if(config.sol.body.actun == 'Actualizacion'){
      var dato = {
        year: config.sol.body.yearb,
        trimestre: config.sol.body.trimestreb,
        lugar: config.sol.files[0].destination,
        nombre: config.sol.files[0].filename,
        nombrer: config.sol.files[0].originalname
      }

      schema.b6.update({year:dato.year,trimestre:dato.trimestre},dato,(err)=>{
        if(err){
          throw err;
        }
        config.res.json('=>');
      });
    }
    if(config.sol.body.actun == 'Actual'){
      if(config.sol.files[0] != undefined){
        var dato = new schema.b6({
          year: config.sol.body.yearb,
          trimestre: config.sol.body.trimestreb,
          lugar: config.sol.files[0].destination,
          nombre: config.sol.files[0].filename,
          nombrer: config.sol.files[0].originalname
        });
      }else{
        var dato = new schema.b6({
          year: config.sol.body.yearb,
          trimestre: config.sol.body.trimestreb,
          lugar: '',
          nombre: 'En este trimestre no se ha generado información al respecto',
          nombrer: ''
        });
      }

      
      schema.b6.findOne({year:config.sol.body.yearb,trimestre:config.sol.body.trimestreb},(err,data)=>{
        if(err){
          throw err;
        }

        if(data == null){
          dato.save((err)=>{
            if(err){
              throw err;
            }
            config.res.json('=>');
          });
        }else{
          config.res.json('<=');
        }
      });
    }
    if(config.sol.body.actun == 'noinfo'){
      var dato = {
        year: config.sol.body.yearb,
        trimestre: config.sol.body.trimestreb,
        lugar: '',
        nombre: 'En este trimestre no se ha generado información al respecto',
        nombrer: ''
      }

      schema.b6.update({year:dato.year,trimestre:dato.trimestre},dato,(err)=>{
        if(err){
          throw err;
        }
        config.res.json('=>');
      });
    }

  }
}

module.exports = b;
