var mongoose = require('mongoose'),
    schema   = require('../../models/db/schema');   

var c = function(config){
  config = config || {};
  console.log(config);
  if(config.sol.body.actun == 'Actualizacion'){
    var dato = {
      year: config.sol.body.yearb,
      trimestre: config.sol.body.trimestreb,
      lugar: config.sol.files[0] == undefined ? '' : config.sol.files[0].destination,
      nombre: config.sol.files[0] == undefined ? '' : config.sol.files[0].filename,
      nombrer: config.sol.files[0] == undefined ? '' : config.sol.files[0].originalname,
      nombre1:config.sol.files[1] == undefined ? '' : config.sol.files[1].filename,
      nombre1r:config.sol.files[1] == undefined ? '' : config.sol.files[1].originalname,
      nombre2: config.sol.files[2] == undefined ? '' : config.sol.files[2].filename,
      nombre2r: config.sol.files[2] == undefined ? '' : config.sol.files[2].originalname    
    }

    schema.c.update({year:dato.year,trimestre:dato.trimestre},dato,(err)=>{
      if(err){
        throw err;
      }
      config.res.json('=>');
    });
  }

  if(config.sol.body.actun == 'Actual'){
    if(config.sol.files[0] != undefined){
      var dato = new schema.c({
        year: config.sol.body.yearb,
        trimestre: config.sol.body.trimestreb,
        lugar: config.sol.files[0] == undefined ? '' : config.sol.files[0].destination,
        nombre: config.sol.files[0] == undefined ? '' : config.sol.files[0].filename,
        nombrer: config.sol.files[0] == undefined ? '' : config.sol.files[0].originalname,
        nombre1:config.sol.files[1] == undefined ? '' : config.sol.files[1].filename,
        nombre1r:config.sol.files[1] == undefined ? '' : config.sol.files[1].originalname,
        nombre2: config.sol.files[2] == undefined ? '' : config.sol.files[2].filename,
        nombre2r: config.sol.files[2] == undefined ? '' : config.sol.files[2].originalname
      });
    }else{
      var dato = new schema.c({
        year: config.sol.body.yearb,
        trimestre: config.sol.body.trimestreb,
        lugar: '',
        nombre: 'En este trimestre no se ha generado información al respecto',
        nombrer: '',
        nombre1: '',
        nombre1r: '',
        nombre2: '',
        nombre2r: ''
      });
    }

    
    schema.c.findOne({year:config.sol.body.yearb,trimestre:config.sol.body.trimestreb},(err,data)=>{
      if(err){
        throw err;
      }

      if(data == null){
        dato.save((err)=>{
          if(err){
            throw err;
          }
          config.res.json('=>');
        });
      }else{
        config.res.json('<=');
      }
    });
  }

  if(config.sol.body.actun == 'noinfo'){
    var dato = {
      year: config.sol.body.yearb,
      trimestre: config.sol.body.trimestreb,
      lugar: '',
      nombre: 'En este trimestre no se ha generado información al respecto',
      nombrer: '',
      nombre1: '',
      nombre1r: '',
      nombre2: '',
      nombre2r: ''
    }

    schema.c.update({year:dato.year,trimestre:dato.trimestre},dato,(err)=>{
      if(err){
        throw err;
      }
      config.res.json('=>');
    });
  }
}

module.exports = c;
