var den     = require('./activa'),
    path    = require('path'),
    fs      = require('fs-extra'),
    multer  = require('multer'),
    storage = multer.diskStorage({destination: (sol, file, fn)=>{
      fn(null, path.join(__dirname,'../../staticos/archivos/archivospdf/'));
    },
    filename: (sol, file, fn)=>{
      fn(null, Date.now() + file.originalname);
    }
    }),
    upload  = multer({ storage: storage}),
    inforese= require('./articulos/infoReservada'),
    b       = require('./articulos/b'),
    c       = require('./articulos/c'),
    d       = require('./articulos/d'),
    e       = require('./articulos/e'),
    f       = require('./articulos/f'),
    g       = require('./articulos/g'),
    h       = require('./articulos/h'),
    i       = require('./articulos/i'),
    j       = require('./articulos/j'),
    k       = require('./articulos/k'),
    l       = require('./articulos/l'),
    m       = require('./articulos/m'),
    n       = require('./articulos/n'),
    o       = require('./articulos/o'),
    p       = require('./articulos/p'),
    q       = require('./articulos/q'),
    r       = require('./articulos/r'),
    s       = require('./articulos/s'),
    t       = require('./articulos/t'),
    ar12t   = require('./articulos/ar12t'),
    ar13t   = require('./articulos/ar13d'),
    ar14t   = require('./articulos/ar14d'),
    moment  = require('moment');
var rutas = function(config){
  config = config || {};
  //Gets
  config.app.get('/inf-reser',den.iniciado,(sol,res,next)=>{
    res.render('secciones/informacionreservada',{data:moment().year()});
  });

  config.app.get('/salir',den.iniciado,(sol,res,next)=>{
    sol.session.destroy();
    res.redirect('/');
  });
  //Post
  //Información reservada
  config.app.post('/informacionReservada',upload.single('reservada-info'),(sol,res,next)=>{
    var data = {
      sol: sol,
      res: res
    }
    fs.move(path.join(__dirname,'../../staticos/archivos/archivospdf/' + sol.file.filename),path.join(__dirname,'../../staticos/archivos/archivospdf/' + sol.body.yearReserva+"/"+sol.body.trimestre + '/' + sol.file.filename ),(err)=>{
      if(err){
        throw err;
      }
    });

    var infoReser = new inforese(data);
  });  

  config.app.post('/informacionNoReservada',(sol,res,next)=>{
    var data = {
      sol: sol,
      res: res
    }
    var noReser = new inforese(data);
  });

  config.app.post('/actualizaReservada',upload.single('actualiza-reservada-info'),(sol,res,next)=>{
    var data = {
      sol: sol,
      res: res
    }

    var actualiza = new inforese(data);
  });

  //---------------------Información----------------//
  //  Esta Seccion fue para información reservada   //
  //------------------------------------------------//

  //---------Rutas B)---------------------------//
  config.app.get('/estruc-o-a-m-f-t-a-c-b',(sol,res,next)=>{
    res.render('secciones/b',{data:moment().year()});
  });

  config.app.post('/insb',upload.any(),(sol,res,next)=>{
    var dato = {
      sol: sol,
      res: res
    }
    var inb = new b(dato);
  });

  //------Rutas c -------------------------//
  config.app.get('/c',(sol,res,next)=>{
    res.render('secciones/c',{data:moment().year()});
  });

  config.app.post('/cs',upload.any(),(sol,res,next)=>{
    var dato = {
      sol: sol,
      res: res
    }

    var cc = new c(dato);
  });

  //--------------rutas d ----------------//
  config.app.get('/d',(sol,res,next)=>{
    res.render('secciones/d',{data:moment().year()});
  });

  config.app.post('/ds',upload.any(),(sol,res,next)=>{
    var dato = {
      sol: sol,
      res: res
    }

    var dd = new d(dato);
  });
  //-----------------rutas e -------------//
  config.app.get('/e',(sol,res,next)=>{
    res.render('secciones/e',{data:moment().year()});
  });

  config.app.post('/es',upload.any(),(sol,res,next)=>{

    var dato = {
      sol: sol,
      res: res
    }

    var ee = new e(dato);
  });

  //---------------rutas f------------------//
  config.app.get('/f',(sol,res,next)=>{
    res.render('secciones/f',{data:moment().year()});
  });

  config.app.post('/fs',upload.any(),(sol,res,next)=>{

    var dato = {
      sol: sol,
      res: res
    }

    var ff = new f(dato);
  });

  //---------------rutas g------------------//
  config.app.get('/g',(sol,res,next)=>{
    res.render('secciones/g',{data:moment().year()});
  });

  config.app.post('/gs',upload.any(),(sol,res,next)=>{

    var dato = {
      sol: sol,
      res: res
    }

    var gg = new g(dato);
  });

  //---------------rutas h------------------//
  config.app.get('/h',(sol,res,next)=>{
    res.render('secciones/h',{data:moment().year()});
  });

  config.app.post('/hs',upload.any(),(sol,res,next)=>{

    var dato = {
      sol: sol,
      res: res
    }

    var hh = new h(dato);
  });

  //---------------rutas i------------------//
  config.app.get('/i',(sol,res,next)=>{
    res.render('secciones/i',{data:moment().year()});
  });

  config.app.post('/is',upload.any(),(sol,res,next)=>{
  
    var dato = {
      sol: sol,
      res: res
    }

    var ii = new i(dato);
  });

  //---------------rutas j------------------//
  config.app.get('/j',(sol,res,next)=>{
    res.render('secciones/j',{data:moment().year()});
  });

  config.app.post('/js',upload.any(),(sol,res,next)=>{
  
    var dato = {
      sol: sol,
      res: res
    }

    var jj = new j(dato);
  });

  //---------------rutas k------------------//
  config.app.get('/k',(sol,res,next)=>{
    res.render('secciones/k',{data:moment().year()});
  });

  config.app.post('/ks',upload.any(),(sol,res,next)=>{
  
    var dato = {
      sol: sol,
      res: res
    }

    var kk = new k(dato);
  });

  //---------------rutas l------------------//
  config.app.get('/l',(sol,res,next)=>{
    res.render('secciones/l',{data:moment().year()});
  });

  config.app.post('/ls',upload.any(),(sol,res,next)=>{
  
    var dato = {
      sol: sol,
      res: res
    }

    var ll = new l(dato);
  });

  //---------------rutas m------------------//
  config.app.get('/m',(sol,res,next)=>{
    res.render('secciones/m',{data:moment().year()});
  });

  config.app.post('/ms',upload.any(),(sol,res,next)=>{
  
    var dato = {
      sol: sol,
      res: res
    }

    var mm = new m(dato);
  });

  //---------------rutas n------------------//
  config.app.get('/n',(sol,res,next)=>{
    res.render('secciones/n',{data:moment().year()});
  });

  config.app.post('/ns',upload.any(),(sol,res,next)=>{
  
    var dato = {
      sol: sol,
      res: res
    }

    var nn = new n(dato);
  });

  //---------------rutas o------------------//
  config.app.get('/o',(sol,res,next)=>{
    res.render('secciones/o',{data:moment().year()});
  });

  config.app.post('/os',upload.any(),(sol,res,next)=>{
  
    var dato = {
      sol: sol,
      res: res
    }

    var oo = new o(dato);
  });

  //---------------rutas p------------------//
  config.app.get('/p',(sol,res,next)=>{
    res.render('secciones/p',{data:moment().year()});
  });

  config.app.post('/ps',upload.any(),(sol,res,next)=>{
  
    var dato = {
      sol: sol,
      res: res
    }

    var pp = new p(dato);
  });

  //---------------rutas q------------------//
  config.app.get('/q',(sol,res,next)=>{
    res.render('secciones/q',{data:moment().year()});
  });

  config.app.post('/qs',upload.any(),(sol,res,next)=>{
  
    var dato = {
      sol: sol,
      res: res
    }

    var qq = new q(dato);
  });

    //---------------rutas r------------------//
  config.app.get('/r',(sol,res,next)=>{
    res.render('secciones/r',{data:moment().year()});
  });

  config.app.post('/rs',upload.any(),(sol,res,next)=>{
  
    var dato = {
      sol: sol,
      res: res
    }

    var rr = new r(dato);
  });

  //---------------rutas s------------------//
  config.app.get('/s',(sol,res,next)=>{
    res.render('secciones/s',{data:moment().year()});
  });

  config.app.post('/ss',upload.any(),(sol,res,next)=>{
  
    var dato = {
      sol: sol,
      res: res
    }

    var ss = new s(dato);
  });

  //---------------rutas t------------------//
  config.app.get('/t',(sol,res,next)=>{
    res.render('secciones/t',{data:moment().year()});
  });

  config.app.post('/ts',upload.any(),(sol,res,next)=>{
  
    var dato = {
      sol: sol,
      res: res
    }

    var tt = new t(dato);
  });

  //---------art 12---------//
  config.app.get('/ar12',(sol,res,next)=>{
    res.render('secciones/ar12',{data:moment().year()});
  });

  config.app.post('/ar12s',upload.any(),(sol,res,next)=>{
  
    var dato = {
      sol: sol,
      res: res
    }

    var ar12ts = new ar12t(dato);
  });

  //---------art 13---------//
  config.app.get('/ar13',(sol,res,next)=>{
    res.render('secciones/ar13',{data:moment().year()});
  });

  config.app.post('/ar13s',upload.any(),(sol,res,next)=>{
  
    var dato = {
      sol: sol,
      res: res
    }

    var ar13ts = new ar13t(dato);
  });

  //---------art 14---------//
  config.app.get('/ar14',(sol,res,next)=>{
    res.render('secciones/ar14',{data:moment().year()});
  });

  config.app.post('/ar14s',upload.any(),(sol,res,next)=>{
  
    var dato = {
      sol: sol,
      res: res
    }

    var ar14ts = new ar14t(dato);
  });
}

module.exports = rutas;
