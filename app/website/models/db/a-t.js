module.exports = {
  a:{
    year: {
      type: Number,
      require: true
    },
    trimestre: {
      type: String,
      require: true
    },
    nombre: {
      type: String,
      require: true
    },
    lugar: {
      type: String
    },
    archivo: {
      type: String
    },
    dato: Number
  },
  b1: {
    year: {
      type: Number,
      require: true
    },
    trimestre: {
      type: String,
      require: true
    },
    lugar: String,
    nombre: String,
    nombrer: String
  },
  b2: {
    year: {
      type: Number,
      require: true
    },
    trimestre: {
      type: String,
      require: true
    },
    lugar: String,
    nombre: String,
    nombrer: String
  },
  b3: {
    year: {
      type: Number,
      require: true
    },
    trimestre: {
      type: String,
      require: true
    },
    lugar: String,
    nombrej1: String,
    nombrejr1: String,
    nombrej2: String,
    nombrej2r: String,
    nombrej3: String,
    nombrej3r: String,
    nombrej4: String,
    nombrej4r: String

  },
  b4: {
    year: {
      type: Number,
      require: true
    },
    trimestre: {
      type: String,
      require: true
    },
    lugar: String,
    nombre: String,
    nombrer: String,
    nombre2: String,
    nombre2r: String
  },
  b5: {
    year: {
      type: Number,
      require: true
    },
    trimestre: {
      type: String,
      require: true
    },
    lugar: String,
    nombre: String,
    nombrer: String
  },
  b6: {
    year: {
      type: Number,
      require: true
    },
    trimestre: {
      type: String,
      require: true
    },
    lugar: String,
    nombre: String,
    nombrer: String
  },
  c: {
    year: {
      type: Number,
      require: true
    },
    trimestre: {
      type: String,
      require: true
    },
    lugar: String,
    nombre: String,
    nombrer: String,
    nombre1: String,
    nombre1r: String,
    nombre2: String,
    nombre2r: String
  },
  d: {
      year: {
        type: Number,
        require: true
      },
      trimestre: {
        type: String,
        require: true
      },
      lugar: String,
      nombre: String,
      nombrer: String
    },
  e: {
    year: {
      type: Number,
      require: true
    },
    trimestre: {
      type: String,
      require: true
    },
    lugar: String,
    nombre: String,
    nombrer: String,
    nombre1: String,
    nombre1r: String
  },
  f: {
    year: {
      type: Number,
      require: true
    },
    trimestre: {
      type: String,
      require: true
    },
    lugar: String,
    nombre: String,
    nombrer: String
  },
  g: {
    year: {
      type: Number,
      require: true
    },
    trimestre: {
      type: String,
      require: true
    },
    lugar: String,
    nombre: String,
    nombrer: String
  },
  h: {
    year: {
      type: Number,
      require: true
    },
    trimestre: {
      type: String,
      require: true
    },
    lugar: String,
    nombre: String,
    nombrer: String
  },
  i: {
    year: {
      type: Number,
      require: true
    },
    trimestre: {
      type: String,
      require: true
    },
    lugar: String,
    nombre: String,
    nombrer: String
  },
  j: {
    year: {
      type: Number,
      require: true
    },
    trimestre: {
      type: String,
      require: true
    },
    lugar: String,
    nombre: String,
    nombrer: String
  },
  k: {
    year: {
      type: Number,
      require: true
    },
    trimestre: {
      type: String,
      require: true
    },
    lugar: String,
    nombre: String,
    nombrer: String
  },
  l: {
    year: {
      type: Number,
      require: true
    },
    trimestre: {
      type: String,
      require: true
    },
    lugar: String,
    nombre: String,
    nombrer: String
  },
  m: {
    year: {
      type: Number,
      require: true
    },
    trimestre: {
      type: String,
      require: true
    },
    lugar: String,
    nombre: String,
    nombrer: String
  },
  n: {
    year: {
      type: Number,
      require: true
    },
    trimestre: {
      type: String,
      require: true
    },
    lugar: String,
    nombre: String,
    nombrer: String
  },
  o: {
    year: {
      type: Number,
      require: true
    },
    trimestre: {
      type: String,
      require: true
    },
    lugar: String,
    nombre: String,
    nombrer: String
  },
  p: {
    year: {
      type: Number,
      require: true
    },
    trimestre: {
      type: String,
      require: true
    },
    lugar: String,
    nombre: String,
    nombrer: String
  },
  q: {
    year: {
      type: Number,
      require: true
    },
    trimestre: {
      type: String,
      require: true
    },
    lugar: String,
    nombre: String,
    nombrer: String
  },
  r: {
    year: {
      type: Number,
      require: true
    },
    trimestre: {
      type: String,
      require: true
    },
    lugar: String,
    nombre: String,
    nombrer: String
  },
  s: {
    year: {
      type: Number,
      require: true
    },
    trimestre: {
      type: String,
      require: true
    },
    lugar: String,
    nombre: String,
    nombrer: String
  },
  t: {
    year: {
      type: Number,
      require: true
    },
    trimestre: {
      type: String,
      require: true
    },
    lugar: String,
    nombre: String,
    nombrer: String
  },  
  ar12: {
    year: {
      type: Number,
      require: true
    },
    trimestre: {
      type: String,
      require: true
    },
    lugar: String,
    nombre: String,
    nombrer: String
  },  
  ar13: {
    year: {
      type: Number,
      require: true
    },
    trimestre: {
      type: String,
      require: true
    },
    lugar: String,
    nombre: String,
    nombrer: String
  }, 
  ar14: {
    year: {
      type: Number,
      require: true
    },
    trimestre: {
      type: String,
      require: true
    },
    lugar: String,
    nombre: String,
    nombrer: String
  }
};
