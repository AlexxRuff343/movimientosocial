var mongoose = require('mongoose'),
    schema   = mongoose.Schema,
    AT       = require('./a-t');

var registro = new schema({
  usuario: {
    type: String,
    require: true
  },
  pass: {
    type: String,
    require: true
  }
});


var Esquemas = {
  registro: mongoose.model('usuarios',registro),
  a: mongoose.model('a',AT.a),
  b1: mongoose.model('b1',AT.b1),
  b2: mongoose.model('b2',AT.b2),
  b3: mongoose.model('b3',AT.b3),
  b4: mongoose.model('b4',AT.b4),
  b5: mongoose.model('b5',AT.b5),
  b6: mongoose.model('b6',AT.b6),
  c: mongoose.model('c',AT.c),
  d: mongoose.model('d',AT.d),
  e: mongoose.model('e',AT.e),
  f: mongoose.model('f',AT.f),
  g: mongoose.model('g',AT.g),
  h: mongoose.model('h',AT.h),
  i: mongoose.model('i',AT.i),
  j: mongoose.model('j',AT.j),
  k: mongoose.model('k',AT.k),
  l: mongoose.model('l',AT.l),
  m: mongoose.model('m',AT.m),
  n: mongoose.model('n',AT.n),
  o: mongoose.model('o',AT.o),
  p: mongoose.model('p',AT.p),
  q: mongoose.model('q',AT.q),
  r: mongoose.model('r',AT.r),
  s: mongoose.model('s',AT.s),
  t: mongoose.model('t',AT.t),
  ar12: mongoose.model('ar12',AT.ar12),
  ar13: mongoose.model('ar13',AT.ar13),
  ar14: mongoose.model('ar14',AT.ar14),
}

module.exports = Esquemas;
